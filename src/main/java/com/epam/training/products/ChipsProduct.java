package com.epam.training.products;

public class ChipsProduct extends Product {
    protected ChipsProduct(double clientHappiness) {
        super(clientHappiness);
    }

    public double countHappiness() {
        return mainProductEffect;
    }

    public void setMainProductEffect(double clientHappiness) {
        mainProductEffect = clientHappiness * 0.05;
    }
}
