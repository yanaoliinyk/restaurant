package com.epam.training.products;

public class HotDogProduct extends Product {

    protected HotDogProduct(double clientHappiness) {
        super(clientHappiness);
    }

    public void setMainProductEffect(double clientHappiness) {
        mainProductEffect = 2;
    }

}
