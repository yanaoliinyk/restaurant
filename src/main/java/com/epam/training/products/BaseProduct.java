package com.epam.training.products;

public abstract class BaseProduct {
    protected double mainProductEffect;

    public abstract double countHappiness();

    public abstract double getMainProductEffect();
}
