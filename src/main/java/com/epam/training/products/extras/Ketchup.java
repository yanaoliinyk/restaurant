package com.epam.training.products.extras;

import com.epam.training.products.BaseProduct;

public class Ketchup extends ExtraDecorator {
    protected Ketchup(BaseProduct baseProduct) {
        super(baseProduct);
    }

    public double countHappiness() {
        return  baseProduct.countHappiness() + baseProduct.getMainProductEffect();
    }
}
