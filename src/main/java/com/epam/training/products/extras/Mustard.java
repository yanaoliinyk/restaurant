package com.epam.training.products.extras;

import com.epam.training.products.BaseProduct;

public class Mustard extends ExtraDecorator {

    protected Mustard(BaseProduct baseProduct) {
        super(baseProduct);
    }

    public double countHappiness() {
        return baseProduct.countHappiness() + 1 - getMainProductEffect();
    }
}
