package com.epam.training.products.extras;

import com.epam.training.exceptions.NoSuchProductException;
import com.epam.training.products.BaseProduct;

public abstract class ExtraDecorator extends BaseProduct {
    BaseProduct baseProduct;

    protected ExtraDecorator(BaseProduct baseProduct) {
        this.baseProduct = baseProduct;
    }

    public double getMainProductEffect() {
        return baseProduct.getMainProductEffect();
    }

    public static ExtraDecorator chooseExtra(ExtraType type, BaseProduct baseProduct) {
        switch (type) {
            case EXTRA_KETCHUP:
                return new Ketchup(baseProduct);
            case EXTRA_MUSTARD:
                return new Mustard(baseProduct);
            default:
                throw new NoSuchProductException("There was no such extra");
        }
    }
}
