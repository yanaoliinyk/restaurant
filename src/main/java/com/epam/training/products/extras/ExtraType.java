package com.epam.training.products.extras;

public enum ExtraType {
    EXTRA_KETCHUP,
    EXTRA_MUSTARD
}
