package com.epam.training.products;

import com.epam.training.exceptions.NoSuchProductException;

public abstract class Product extends BaseProduct {

    public static Product chooseProduct(ProductType type, double clientHappiness) {
        switch (type) {
            case HOTDOG:
                return new HotDogProduct(clientHappiness);
            case CHIPS:
                return new ChipsProduct(clientHappiness);
            default:
                throw new NoSuchProductException("There was no such product");
        }
    }

    protected Product(double clientHappiness){
        setMainProductEffect(clientHappiness);
    }

    public abstract void setMainProductEffect(double clientHappiness);

    public double countHappiness() {
        return mainProductEffect;
    }

    public double getMainProductEffect() {
        return mainProductEffect;
    }
}
