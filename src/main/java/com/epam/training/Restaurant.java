package com.epam.training;

import java.util.ArrayList;
import java.util.Random;

public class Restaurant implements Runnable {
    private ArrayList<Order> orders = new ArrayList<>();
    private volatile boolean isWorking = true;
    private Random random = new Random();

    public void addOrder(Order order) {
        orders.add(order);
    }

    private void removeOrder(Order order) {
        orders.remove(order);
    }

    private void cookOrder(Order order) {
        double extraHappiness = order.process();
        try {
            Thread.sleep(random.nextInt(5000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        order.getClient().notify(extraHappiness);
    }

    public void stopWorking() {
        isWorking = false;
    }

    @Override
    public void run() {
        while (isWorking || !orders.isEmpty()) {
            if (!orders.isEmpty()) {
                Order order = orders.get(0);
                cookOrder(order);
                removeOrder(order);
            }
        }
    }
}
