package com.epam.training;

public class Client {
    private String name;
    private double happiness;

    public Client(String name, double happiness) {
        this.name = name;
        this.happiness = happiness;
        System.out.println(name + " is now this happy: " + happiness);
    }

    public double getHappiness() {
        return happiness;
    }

    public void makeOrder(Order order, Restaurant restaurant){
        restaurant.addOrder(order);
    }

    public void notify(double extraHappiness){
        consume(extraHappiness);
    }

    public void consume(double happiness) {
        this.happiness += happiness;
        System.out.println(name + " becomes this happy " + this.happiness);
    }
}
