package com.epam.training.exceptions;

public class ProductNotSpecifiedException extends RuntimeException {
    public ProductNotSpecifiedException(String message) {
        super(message);
    }
}
