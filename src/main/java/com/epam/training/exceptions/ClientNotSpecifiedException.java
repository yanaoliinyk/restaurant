package com.epam.training.exceptions;

public class ClientNotSpecifiedException extends RuntimeException {
    public ClientNotSpecifiedException(String message) {
        super(message);
    }
}
