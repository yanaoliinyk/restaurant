package com.epam.training;

import com.epam.training.exceptions.ClientNotSpecifiedException;
import com.epam.training.exceptions.ProductNotSpecifiedException;
import com.epam.training.products.BaseProduct;
import com.epam.training.products.Product;
import com.epam.training.products.ProductType;
import com.epam.training.products.extras.ExtraDecorator;
import com.epam.training.products.extras.ExtraType;

import java.util.ArrayList;

public class Order {
    private Client client;
    private ProductType productType;
    private ArrayList<ExtraType> extraTypes;

    public Client getClient() {
        return client;
    }

    private void setClient(Client client) {
        this.client = client;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public void setExtraTypes(ArrayList<ExtraType> extraTypes) {
        this.extraTypes = extraTypes;
    }

    public double process(){
        BaseProduct product = Product.chooseProduct(productType, client.getHappiness());
        for (ExtraType extraType : extraTypes) {
            product = ExtraDecorator.chooseExtra(extraType, product);
        }
        return product.countHappiness();
    }

    public static class Builder {
        private Client client;
        private ProductType productType;
        private ArrayList<ExtraType> extraTypes = new ArrayList<>();

        public Builder withClient(Client client) {
            this.client = client;
            return this;
        }

        public Builder withProduct(ProductType productType) {
            this.productType = productType;
            return this;
        }

        public Builder withExtra(ExtraType extraType) {
            extraTypes.add(extraType);
            return this;
        }

        public Order build() {
            if (null == client) {
                throw new ClientNotSpecifiedException("client was not specified for order");
            }
            if (null == productType) {
                throw new ProductNotSpecifiedException("product was not specified for order");
            }
            Order order = new Order();
            order.setClient(client);
            order.setProductType(productType);
            order.setExtraTypes(extraTypes);

            return order;
        }
    }
}
