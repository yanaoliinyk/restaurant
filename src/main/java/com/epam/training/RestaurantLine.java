package com.epam.training;

import com.epam.training.products.ProductType;
import com.epam.training.products.extras.ExtraType;

public class RestaurantLine {

    public static void main(String[] args) throws InterruptedException {
        Restaurant restaurant = new Restaurant();
        Thread thread = new Thread(restaurant);
        thread.start();

        Client client1 = new Client("Client1", 15);
        Order order1 = new Order.Builder()
                .withClient(client1)
                .withProduct(ProductType.HOTDOG)
                .withExtra(ExtraType.EXTRA_KETCHUP)
                .withExtra(ExtraType.EXTRA_MUSTARD)
                .build();
        client1.makeOrder(order1, restaurant);

        Client client2 = new Client("Client2", 100);
        Order order2 = new Order.Builder()
                .withClient(client2)
                .withProduct(ProductType.CHIPS)
                .withExtra(ExtraType.EXTRA_KETCHUP)
                .withExtra(ExtraType.EXTRA_KETCHUP)
                .build();
        client2.makeOrder(order2, restaurant);

        Order order3 = new Order.Builder()
                .withClient(client1)
                .withProduct(ProductType.CHIPS)
                .build();
        client1.makeOrder(order3, restaurant);

        restaurant.stopWorking();
    }
}
